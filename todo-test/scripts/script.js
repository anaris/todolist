// Tutaj dodacie zmienne globalne do przechowywania elementów takich jak np. lista czy input do wpisywania nowego todo
let $list, $form, $addTodo, $input, $popupInput, $modal, $span, $closeModal, $doneBtn, $cancelBtn, currentId;
const initialList = ['Dzisiaj robię usuwanie', 'Nakarm psa'];

function main() {
  prepareDOMElements();
  prepareDOMEvents();
  //prepareInitialList();
  getListFromServer();
}

function prepareDOMElements() {
  // To będzie idealne miejsce do pobrania naszych elementów z drzewa DOM i zapisanie ich w zmiennych
  $list = document.getElementById('list');
  $addTodo = document.getElementById('addTodo');
  $input = document.getElementById('myInput');
  $popupInput = document.getElementById('popupInput');
  $modal = document.querySelector('.modal');
  $myModal = document.querySelector('#myModal');
  $span = document.getElementById('closePopup');
  $form = document.querySelector('form');
  $closeModal = document.getElementById('closePopup');
  $doneBtn = document.getElementById('btn__done');
  $cancelBtn = document.getElementById('btn__cancel');
}

function prepareDOMEvents() {
  // Przygotowanie listenerów
  $list.addEventListener('click', listClickManager);
  $addTodo.addEventListener('click', addNewTodoToList);
  $form.addEventListener('submit', keyPressed);
  $closeModal.addEventListener('click', closePopup);
  $doneBtn.addEventListener('click', acceptChangeHandler);
  $cancelBtn.addEventListener('click', declineChanges);
}

async function getListFromServer() {
    $list.innerHTML = '';
    try {
        let response = await axios.get('http://195.181.210.249:3000/todo/');
        if (response.status === 200) {
            response.data.forEach(todo => {
                addNewElementToList(todo.title, todo.id);
            });
        }
    }
    catch (e) {
        console.log(e);
    }
}

function prepareInitialList() {
  // Tutaj utworzymy sobie początkowe todosy. Mogą pochodzić np. z tablicy
  initialList.forEach(todo => {
    addNewElementToList(todo);
  });
}

function addNewElementToList(id, title) {
  //obsługa dodawanie elementów do listy
  // $list.appendChild(createElement('nowy', 2))
  const newElement = createElement(id, title);
  addNewTodoToList(newElement);
}

function createElement(title, id) {
  // Tworzyc reprezentacje DOM elementu return newElement
  // return newElement
  
  const newElement = document.createElement('li');
  newElement.setAttribute('data-id', id);

  const divBtn = document.createElement('div');
  divBtn.className = 'div-btn';

  const titleElement = document.createElement('span');
  titleElement.innerText = title;
  titleElement.className = 'span-todo';

  const delBtn = document.createElement('button');
  delBtn.innerText = 'Delete';
  delBtn.className = 'btn-delete';

  const editBtn = document.createElement('button');
  editBtn.innerText = 'Edit';
  editBtn.className = 'btn-edit';

  const doneBtn = document.createElement('button');
  doneBtn.innerText = 'Done';
  doneBtn.className = 'btn-done';

  newElement.appendChild(titleElement);
  newElement.appendChild(divBtn);
  divBtn.appendChild(delBtn);
  divBtn.appendChild(editBtn);
  divBtn.appendChild(doneBtn);

  return newElement;
}

function addNewTodoToList() {
    if ($input.value.trim() !== "") {
        axios.post('http://195.181.210.249:3000/todo/', { title: $input.value }).then((response) => {
            if (response.data.status === 0) {
                getListFromServer();
                console.log($input.value);
            }
        });
        $input.value = '';
    }
    console.log($input.value);
}

function listClickManager(event) {
  // Rozstrzygnięcie co dokładnie zostało kliknięte i wywołanie odpowiedniej funkcji
  // event.target.parentElement.id
  let id = event.target.parentElement.dataset.id;
  let title = event.target.parentElement.title;
  if (event.target.className === 'btn-edit') { 
    editListElement(id, title) }
  else if (event.target.className === 'btn-delete') {
      removeListElement(id);}
    else (event.target.className === 'btn-done') 
       markElementAsDone(id, title);
}

function removeListElement(id) {
  // Usuwanie elementu z listy
  axios.delete('http://195.181.210.249:3000/todo/' + id).then((response) => {
      if (response.data.status === 0) {
        getListFromServer();
      }
  });
}

function editListElement(id, title) {
  // Pobranie informacji na temat zadania
  // Umieść dane w popupie

  openPopup();
  addDataToPopup(id, title);
}

function addDataToPopup(id, title) {
  // umieść informacje w odpowiednim miejscu w popupie
  $popupInput.value = title;
  currentId = id;
}

function acceptChangeHandler() {
  // pobierz dane na temat zadania z popupu (id, nowyTitle, nowyColor ...)
  // Następnie zmodyfikuj element listy wrzucając w niego nowyTitle, nowyColor...
  // closePopup()
 
  axios.put('http://195.181.210.249:3000/todo/' + currentId, {title: $popupInput.value}).then((response) => {
      if (response.data.status === 0) {
          getListFromServer();
      };
  });
  closePopup();
}

function openPopup() {
  // Otwórz popup
    $modal.classList.toggle('modal-show');
}

function closePopup() {
  // Zamknij popup
  $modal.classList.toggle('modal-show');
}

function declineChanges() { //niepotrzebna raczej
  // closePopup()
  $popupInput.value = '';
}

function markElementAsDone(id, title) {
  //zaznacz element jako wykonany (podmień klasę CSS)
  currentId = document.querySelector('[data-id="' + id + '"]') 
  $input.value = title;
  console.log(currentId);
  $input.value.classList.toggle('checked');
}

function keyPressed(event) {
  event.preventDefault();
  { if(event.keyCode === 13) {
    addNewTodoToList();
    }
}
}

document.addEventListener('DOMContentLoaded', main);
