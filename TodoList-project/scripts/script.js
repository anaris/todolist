// Tutaj dodacie zmienne globalne do przechowywania elementów takich jak np. lista czy input do wpisywania nowego todo
let $list, $modal, $buttonModal, $buttonConsole, $addBtn, $input, $form;
const initialList = ['Dzisiaj robię usuwanie', 'Nakarm psa', 'pływalnia'];

function main() {
  prepareDOMElements();
  prepareDOMEvents();
  prepareInitialList();
}

function prepareDOMElements() {
  // To będzie idealne miejsce do pobrania naszych elementów z drzewa DOM i zapisanie ich w zmiennych
  $list = document.getElementById('list');
  $modal = document.querySelector('#modal');
  $input = document.querySelector('#myInput');
  $addBtn = document.querySelector('.addBtn');

  /*for (i = 0; i < $list.clientHeight; i++) {
    let span = document.createElement ("span");
    let txt = document.createTextNode ("\u00D7");
    span.className = "close";
    span.appendChild(Text);
    $list[i].appendChild(span);
  }*/
}

function prepareDOMEvents() {
  // Przygotowanie listenerów
  $list.addEventListener('click', listClickManager);
}

function prepareInitialList() {
  // Tutaj utworzymy sobie początkowe todosy. Mogą pochodzić np. z tablicy
  initialList.forEach(todo => {
    addNewElementToList(todo);
  });
}

function addNewElementToList(title   /* Title, author, id */) {
  //obsługa dodawanie elementów do listy
  // $list.appendChild(createElement('nowy', 2))
  const newElement = createElement(title);
  $list.appendChild(newElement);
}

function createElement(title /* Title, author, id */) {
  // Tworzyc reprezentacje DOM elementu return newElement
  // return newElement
  const newElement = document.createElement('li');
  newElement.innerText = title;

  return newElement;
}

function listClickManager(event) {
  // Rozstrzygnięcie co dokładnie zostało kliknięte i wywołanie odpowiedniej funkcji
  // event.target.parentElement.id
  if (event.target.className === 'edit') { editListElement(id) }
  else if (event.target.className === 'delete') { removeListElement(id)}
  else if (event.target.className === 'mark-as-done') { markListElement(id)}
}

function removeListElement(id) {
  // Usuwanie elementu z listy
}

function editListElement(id) {
  // Pobranie informacji na temat zadania
  // Umieść dane w popupie
}

function markListElement(id) {

}

function addDataToPopup(/* Title, author, id */) {
  // umieść informacje w odpowiednim miejscu w popupie
}

function acceptChangeHandler() {
  // pobierz dane na temat zadania z popupu (id, nowyTitle, nowyColor ...)
  // Następnie zmodyfikuj element listy wrzucając w niego nowyTitle, nowyColor...
  // closePopup()
}

function openPopup() {
  // Otwórz popup
}

function closePopup() {
  // Zamknij popup
}

function declineChanges() { //niepotrzebna raczej
  // closePopup()
}

function markElementAsDone(/* id */) {
  //zaznacz element jako wykonany (podmień klasę CSS)
}

document.addEventListener('DOMContentLoaded', main);
